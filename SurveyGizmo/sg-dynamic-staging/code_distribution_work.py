#!/usr/local/bin/python3
'''Create all of the code distribution files and folders as needed by the homegrown CI/CD
    Note: This must be run as a user that can add in the files and folders'''

import os
import shutil
import re


def create_distribution_folder(name, uid=504, gid=505, mode_octal=0o755):
    '''create the folder /opt/surveygizmo/$ENVNAME, chown to rollout
       copy in the latest build from production and etc folder
       symlink current to that build
    '''

    path = f'/opt/surveygizmo/{name}'
    prod_path = '/opt/surveygizmo/appv4'
    link_path = f'{prod_path}/current'

    linked_dir = os.readlink(link_path)
    cp_path = os.path.join(os.path.dirname(link_path), linked_dir)
    shutil.copytree(cp_path, f'{path}/{linked_dir}', symlinks=True)

    build_path = os.path.join(path, linked_dir)
    os.chown(build_path, uid, gid, follow_symlinks=False)
    os.symlink(build_path, f'{path}/current', target_is_directory=True)

    shutil.copytree(f'{prod_path}/etc', f'{path}/etc')
    os.chown(f'{path}/etc', uid, gid, follow_symlinks=False)
    os.chmod(path, mode_octal)


def create_distribute_file(name):
    '''Create /usr/local/distribute/etc/$ENVNAME.sh based on the existing templates'''

    dist_file = f'/usr/local/distribute/etc/{name}.sh'
    with open(dist_file, 'x') as dfile:
        print('REGIONS=\'us-east-1\'', file=dfile)
        print('TIERS=\'Everything,Web\'', file=dfile)
        print('DIST_BASE=\'/opt/surveygizmo\'', file=dfile)
        print('RSYNC_EXCLUDES=\'*/etc/*\'', file=dfile)
        print('RSYNC_LOG_DIR="/var/log/install_package"', file=dfile)
        print('DIST_USER=rollout', file=dfile)
        print('TARBALL_DIR=/opt/surveygizmo/packaging', file=dfile)
        print('CONF_LINKS="etc/config.ini:application/config.ini etc/config.php:surveyslave/config.php"', file=dfile)
        print(f'''POST_VERSION_CHANGE="/usr/local/sbin/ssh_menu_root {name} Everything cmd \'service httpd graceful\'; /usr/local/sbin/ssh_menu_root {name} Web cmd \'service httpd graceful\'"''', file=dfile)


def create_qa_auto_install(name, uid=504, gid=505, mode=0o755):
    '''create /opt/surveygizmo/packaging/auto/$ENVNAME
    chown to rollout:rollout'''

    path = f'/opt/surveygizmo/packaging/auto/{name}'
    os.mkdir(path)
    os.chmod(path, mode)
    os.chown(path, uid, gid, follow_symlinks=False)


def append_rsyncd(name):
    '''Append ENVNAME to the end of /etc/rsyncd.conf'''

    file_path = '/etc/rsyncd.conf'

    with open(file_path, 'a') as rsync_file:
        print(f'\n[{name}]', file=rsync_file)
        print(f'    path = /opt/surveygizmo/{name}', file=rsync_file)
        print(f'    exclude = /etc', file=rsync_file)
        print(f'    comment = Dynamic Staging {name}', file=rsync_file)


def destroy_distribution_folder(name):
    '''delete the folder at /opt/surveygizmo/$ENVNAME'''

    path = f'/opt/surveygizmo/{name}'
    shutil.rmtree(path)


def destroy_distribution_file(name):
    '''delete /usr/local/distribute/etc/$ENVNAME.sh'''

    dist_file = f'/usr/local/distribute/etc/{name}.sh'
    os.remove(dist_file)


def destroy_qa_auto_install(name):
    '''delete /opt/surveygizmo/packaging/auto/$ENVNAME'''

    path = f'/opt/surveygizmo/packaging/auto/{name}'
    shutil.rmtree(path)


def delete_rsyncd_entry(name):
    '''open /etc/rsyncd.conf delete the 4 lines for the env name'''

    file_path = '/etc/rsyncd.conf'

    env_name_pattern = re.compile(f'^\[{name}\]')
    full_file = open(file_path).readlines()
    delete_index_initial = 0

    for line in full_file:
        env_match = re.match(env_name_pattern, line)
        if env_match:
            delete_index_initial = full_file.index(line)

    inital_del = delete_index_initial - 1
    final_del = delete_index_initial + 3

    open(file_path, 'w').writelines(full_file[:inital_del])
    open(file_path, 'a').writelines(full_file[final_del:])
