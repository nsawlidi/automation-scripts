#!/usr/local/bin/python3
''' Initial tie together script.
    Modules from all of the pieces should run sequentially
    Allowing for each piece to spin up first.
    I'm omitting the HAProxy work (too much risk for testing)
    and Puppet work (needs design)
    '''

import code_distribution_work
import ec2_work
import dns_work
import cloudfront_work

TEST_NAME = 'nicktestdeleteme3'


def test_create(NAME):
    '''Test tying the creation stuff together'''
    print("######### BEGIN SCRIPT ##########\n")

    print("\n### START Code Distribution Work")
    code_distribution_work.create_distribution_folder(NAME)
    print("Distribution folder created")
    code_distribution_work.create_distribute_file(NAME)
    print("Distribution file created")
    code_distribution_work.create_qa_auto_install(NAME)
    print("QA Auto folder created")
    code_distribution_work.append_rsyncd(NAME)
    print("Appending to rsyncd is done")
    print("### END Code Distribution Work \n")

    print("\n### START EC2 spot instance req")
    ec2_work.req_spot_instances(NAME)
    print("### END EC2 spot instance req\n")

    print("\n### INSERT HAPROXY HERE ###\n")

    print("\n### BEGIN DNS WORK")
    everything_ip = ec2_work.get_server_ips('StagingHaProxy', 'HaProxy')
    print("EVERYTHING IP: " + str(everything_ip[0]))
    web_ip = ec2_work.get_server_ips('StagingHaProxyWeb', 'HaProxy')
    print("WEB IP: " + str(web_ip[0]))
    dns_work.create_route53_entries(NAME, everything_ip[0], web_ip[0])
    print("Route53 Entries Created")
    print("### END DNS WORK\n")

    print("\n### INSERT COUDFLARE HERE ###\n")

    print("\n### BEGIN CloudFront Work")
    cloudfront_work.create_cloudfront(NAME)
    print("### END CloudFront Work\n")

    print("######### END CREATION SCRIPT ###########")


if __name__ == "__main__":
    test_create(TEST_NAME)
