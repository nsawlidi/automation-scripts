#!/usr/local/bin/python3
'''CloudFront Helper Functions'''

import boto3

CLIENT = boto3.client('cloudfront')

def create_cloudfront(name):
    '''create the cloudfront distribution that sits in front of Web'''

    response = CLIENT.create_distribution(
        DistributionConfig={
            'CallerReference': f'{name}',
            'Aliases': {
                'Quantity': 1,
                'Items': [f'{name}www.surveygizmo.com',]
            },
            'Origins': {
                'Quantity': 2,
                'Items': [
                    {
                        'Id': f'Custom-{name}www.thing.net',
                        'DomainName': f'{name}www.thing.net',
                        'CustomOriginConfig': {
                            'HTTPPort': 80,
                            'HTTPSPort': 443,
                            'OriginProtocolPolicy': 'match-viewer',
                            'OriginSslProtocols': {
                                'Quantity': 1,
                                'Items': [
                                    'TLSv1.2',
                                ]
                            },
                            'OriginReadTimeout': 30,
                            'OriginKeepaliveTimeout': 5
                        }
                    },
                    {
                        'Id': f'Custom-{name}www.thing.net/s3',
                        'DomainName': f'{name}www.thing.net',
                        'OriginPath': '/s3',
                        'CustomOriginConfig': {
                            'HTTPPort': 80,
                            'HTTPSPort': 443,
                            'OriginProtocolPolicy': 'match-viewer',
                            'OriginSslProtocols': {
                                'Quantity': 1,
                                'Items': [
                                    'TLSv1.2',
                                ]
                            },
                            'OriginReadTimeout': 30,
                            'OriginKeepaliveTimeout': 5
                        }
                    },
                ]
            },
            'DefaultCacheBehavior': {
                'TargetOriginId': f'Custom-{name}www.thing.net',
                'ForwardedValues': {
                    'QueryString': False,
                    'Cookies': {
                        'Forward': 'none',
                    },
                    'Headers': {
                        'Quantity': 2,
                        'Items': [
                            'Cloudfront-Viewer-Country',
                            'Flubby-Country',
                        ]
                    },
                },
                'TrustedSigners': {
                    'Enabled': False,
                    'Quantity': 0,
                },
                'ViewerProtocolPolicy': 'redirect-to-https',
                'MinTTL': 0,
                'AllowedMethods': {
                    'Quantity': 2,
                    'Items': [
                        'GET', 'HEAD',
                    ],
                    'CachedMethods': {
                        'Quantity': 2,
                        'Items': [
                            'GET', 'HEAD'
                        ]
                    }
                },
                'SmoothStreaming': False,
                'DefaultTTL': 86400,
                'MaxTTL': 31536000,
                'Compress': False,
            },
            'CacheBehaviors': {
                'Quantity': 6,
                'Items': [
                    {
                        'PathPattern': '/collab*',
                        'TargetOriginId': f'Custom-{name}www.thing.net',
                        'ForwardedValues': {
                            'QueryString': True,
                            'Cookies': {
                                'Forward': 'none',
                            },
                            'Headers': {
                                'Quantity': 1,
                                'Items': [
                                    '*',
                                ]
                            },
                            'QueryStringCacheKeys': {
                                'Quantity': 1,
                                'Items': [
                                    '*',
                                ]
                            }
                        },
                        'TrustedSigners': {
                            'Enabled': False,
                            'Quantity': 0,
                        },
                        'ViewerProtocolPolicy': 'allow-all',
                        'MinTTL': 0,
                        'AllowedMethods': {
                            'Quantity': 7,
                            'Items': [
                                'GET',
                                'HEAD',
                                'POST',
                                'PUT',
                                'PATCH',
                                'OPTIONS',
                                'DELETE'
                            ],
                            'CachedMethods': {
                                'Quantity': 3,
                                'Items': [
                                    'GET', 'HEAD', 'OPTIONS'
                                ]
                            }
                        },
                        'SmoothStreaming': False,
                        'DefaultTTL': 0,
                        'MaxTTL': 0,
                        'Compress': False,
                    },
                    {
                        'PathPattern': '/runtimejs/intercept/intercept.js',
                        'TargetOriginId': f'Custom-{name}www.thing.net/s3',
                        'ForwardedValues': {
                            'QueryString': False,
                            'Cookies': {
                                'Forward': 'none',
                            },
                            'Headers': {
                                'Quantity':0,
                            },
                            'QueryStringCacheKeys': {
                                'Quantity': 0,
                            }
                        },
                        'TrustedSigners': {
                            'Enabled': False,
                            'Quantity': 0,
                        },
                        'ViewerProtocolPolicy': 'allow-all',
                        'MinTTL': 7200,
                        'AllowedMethods': {
                            'Quantity': 2,
                            'Items': [
                                'GET', 'HEAD',
                            ],
                            'CachedMethods': {
                                'Quantity': 2,
                                'Items': [
                                    'GET', 'HEAD',
                                ]
                            }
                        },
                        'SmoothStreaming': False,
                        'DefaultTTL': 7200,
                        'MaxTTL': 10000,
                        'Compress': True,
                    },
                    {
                        'PathPattern': '/s3/*/runtime*',
                        'TargetOriginId': f'Custom-{name}www.thing.net',
                        'ForwardedValues': {
                            'QueryString': False,
                            'Cookies': {
                                'Forward': 'none',
                            },
                            'Headers': {
                                'Quantity': 0,
                            },
                            'QueryStringCacheKeys': {
                                'Quantity': 0,
                            }
                        },
                        'TrustedSigners': {
                            'Enabled': False,
                            'Quantity': 0,
                        },
                        'ViewerProtocolPolicy': 'allow-all',
                        'MinTTL': 0,
                        'AllowedMethods': {
                            'Quantity': 2,
                            'Items': [
                                'GET', 'HEAD',
                            ],
                            'CachedMethods': {
                                'Quantity': 2,
                                'Items': [
                                    'GET', 'HEAD',
                                ]
                            }
                        },
                        'SmoothStreaming': False,
                        'DefaultTTL': 86400,
                        'MaxTTL': 31536000,
                        'Compress': True,
                    },
                    {
                        'PathPattern': '/*/*runtime*/*',
                        'TargetOriginId': f'Custom-{name}www.thing.net/s3',
                        'ForwardedValues': {
                            'QueryString': False,
                            'Cookies': {
                                'Forward': 'none',
                                'WhitelistedNames': {
                                    'Quantity': 0,
                                }
                            },
                            'Headers': {
                                'Quantity': 0,
                            },
                            'QueryStringCacheKeys': {
                                'Quantity': 0,
                            }
                        },
                        'TrustedSigners': {
                            'Enabled': False,
                            'Quantity': 0,
                        },
                        'ViewerProtocolPolicy': 'allow-all',
                        'MinTTL': 0,
                        'AllowedMethods': {
                            'Quantity': 2,
                            'Items': [
                                'GET', 'HEAD',
                            ],
                            'CachedMethods': {
                                'Quantity': 2,
                                'Items': [
                                    'GET', 'HEAD',
                                ]
                            }
                        },
                        'SmoothStreaming': False,
                        'DefaultTTL': 86400,
                        'MaxTTL': 31536000,
                        'Compress': True,
                    },
                    {
                        'PathPattern': '/s3/*/public/*',
                        'TargetOriginId': f'Custom-{name}www.thing.net',
                        'ForwardedValues': {
                            'QueryString': False,
                            'Cookies': {
                                'Forward': 'none',
                                'WhitelistedNames': {
                                    'Quantity': 123,
                                    'Items': [
                                        'string',
                                    ]
                                }
                            },
                            'Headers': {
                                'Quantity': 0,
                            },
                            'QueryStringCacheKeys': {
                                'Quantity': 0,
                            }
                        },
                        'TrustedSigners': {
                            'Enabled': False,
                            'Quantity': 0,
                        },
                        'ViewerProtocolPolicy': 'allow-all',
                        'MinTTL': 0,
                        'AllowedMethods': {
                            'Quantity': 7,
                            'Items': [
                                'GET',
                                'HEAD',
                                'POST',
                                'PUT',
                                'PATCH',
                                'OPTIONS',
                                'DELETE',
                            ],
                            'CachedMethods': {
                                'Quantity': 2,
                                'Items': [
                                    'GET', 'HEAD',
                                ]
                            }
                        },
                        'SmoothStreaming': False,
                        'DefaultTTL': 86400,
                        'MaxTTL': 31536000,
                        'Compress': True,
                    },
                    {
                        'PathPattern': '*/public/images/*',
                        'TargetOriginId': f'Custom-{name}www.thing.net/s3',
                        'ForwardedValues': {
                            'QueryString': False,
                            'Cookies': {
                                'Forward': 'none',
                                'WhitelistedNames': {
                                    'Quantity': 0,
                                }
                            },
                            'Headers': {
                                'Quantity': 0,
                            },
                            'QueryStringCacheKeys': {
                                'Quantity': 0,
                            }
                        },
                        'TrustedSigners': {
                            'Enabled': False,
                            'Quantity': 0,
                        },
                        'ViewerProtocolPolicy': 'allow-all',
                        'MinTTL': 0,
                        'AllowedMethods': {
                            'Quantity': 7,
                            'Items': [
                                'GET',
                                'HEAD',
                                'POST',
                                'PUT',
                                'PATCH',
                                'OPTIONS',
                                'DELETE',
                            ],
                            'CachedMethods': {
                                'Quantity': 2,
                                'Items': [
                                    'GET', 'HEAD',
                                ]
                            }
                        },
                        'SmoothStreaming': False,
                        'DefaultTTL': 86400,
                        'MaxTTL': 31536000,
                        'Compress': True,
                    },
                ]
            },
            'CustomErrorResponses': {
                'Quantity': 0,
            },
            'Comment': 'CREATED BY DYNAMIC STAGING SCRIPT',
            'Logging': {
                'Enabled': False,
                'IncludeCookies': False,
                'Bucket': 'string',
                'Prefix': 'string'
            },
            'PriceClass': 'PriceClass_100',
            'Enabled': True,
            'ViewerCertificate': {
                'CloudFrontDefaultCertificate': False,
                'ACMCertificateArn': 'arn:aws:acm:us-east-1:REDACTED:certificate/REDACTED',
                'SSLSupportMethod': 'sni-only',
                'MinimumProtocolVersion': 'TLSv1.1_2016',
            },
            'Restrictions': {
                'GeoRestriction': {
                    'RestrictionType': 'none',
                    'Quantity': 0,
                }
            },
            'HttpVersion': 'http1.1',
            'IsIPV6Enabled': True
        }
    )
    print(response)


def disable_cloudfront(name):
    '''disable the cloudfront distribution'''

    alias_name = f'{name}www.thing.net'
    dist_id = ''

    response = CLIENT.list_distributions()
    for item in response["DistributionList"]["Items"]:
        if item["Aliases"]["Quantity"] > 0:
            for al in item["Aliases"]["Items"]:
                if al == alias_name:
                    dist_id = item['Id']

    dist_full_config = CLIENT.get_distribution_config(Id=dist_id)
    dist_config = dist_full_config['DistributionConfig']
    dist_etag = dist_full_config['ETag']
    dist_config['Enabled'] = False
    CLIENT.update_distribution(DistributionConfig=dist_config, Id=dist_id, IfMatch=dist_etag)
