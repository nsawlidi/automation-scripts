#!/usr/local/bin/python3
'''Route53 and Cloudflare helper functions'''

import boto3

CLIENT = boto3.client('route53')
SGNET_R53_ZONEID = 'REDACTED'


def create_route53_entries(name, app_ip_addr, web_ip_addr):
    ''' create the route53 entries on thing.net
     ensure the IP address is the HAProxy '''

    response = CLIENT.change_resource_record_sets(
        HostedZoneId=SGNET_R53_ZONEID,
        ChangeBatch={
            'Comment': f'DYNAMIC STAGING {name}',
            'Changes': [
                {
                    'Action': 'CREATE',
                    'ResourceRecordSet': {
                        'Name': f'{name}app.thing.net.',
                        'Type': 'A',
                        'TTL': 300,
                        'ResourceRecords': [
                            {
                                'Value': f'{app_ip_addr}'
                            },
                        ],
                    }
                },
                {
                    'Action': 'CREATE',
                    'ResourceRecordSet': {
                        'Name': f'{name}.thing.net.',
                        'Type': 'A',
                        'TTL': 300,
                        'ResourceRecords': [
                            {
                                'Value': f'{web_ip_addr}'
                            },
                        ],
                    }
                },
            ]
        }
    )
    print(response)


def destroy_route53_entries(name, app_ip_addr, web_ip_addr):
    ''' delete the thing.net entries '''
    response = CLIENT.change_resource_record_sets(
        HostedZoneId=SGNET_R53_ZONEID,
        ChangeBatch={
            'Comment': f'DYNAMIC STAGING {name}',
            'Changes': [
                {
                    'Action': 'DELETE',
                    'ResourceRecordSet': {
                        'Name': f'{name}app.thing.net.',
                        'Type': 'A',
                        'TTL': 300,
                        'ResourceRecords': [
                            {
                                'Value': f'{app_ip_addr}'
                            },
                        ],
                    }
                },
                {
                    'Action': 'DELETE',
                    'ResourceRecordSet': {
                        'Name': f'{name}.thing.net.',
                        'Type': 'A',
                        'TTL': 300,
                        'ResourceRecords': [
                            {
                                'Value': f'{web_ip_addr}'
                            },
                        ],
                    }
                },
            ]
        }
    )
    print(response)
