#!/opt/python3/bin/python3

from ec2_work import get_server_ips


def create_paths(name):
    app_path = f"{name}app.thing.net"
    www_path = f"{name}www.thing.net"
    print(f"acl {name}-app hdr(Host) -i {app_path}")
    print(f"acl {name}-www hdr(Host) -i {www_path}")


def create_use_backend(name):
    print(f"use_backend {name}app if {name}-app")
    print(f"use_backend {name}www if {name}-www")


def backend_create_app(name):
    ip_list = get_server_ips(name, tier)

    print(f"backend {name}app")
    print("         balance leastconn")
    print("         option httpchk get /s3/status.php")
    print("         http-check expect string online")
    print("         stats enable")
    print("         stats uri /admin?stats")
    for ip in ip_list:
        print(f"         server {ip} {ip}:80 check")
    print(" errorfile     500 /opt/haproxy-ssl/errors/500.html")
    print(" errorfile     502 /opt/haproxy-ssl/errors/500.html")
    print(" errorfile     503 /opt/haproxy-ssl/errors/500.html")


def backend_create_survey(name):
    www_path = f"{name}www.thing.net"
    ip_list = get_server_ips(name, tier)

    print(f"backend {name}www")
    print(f"     redirect prefix {www_path} code 301 if" + " { hdr(host) -i edu.surveygizmo.com || hdr(host) -i surveygizmo.com }")
    print("     balance leastconn")
    print("     option httpchk get /s3/status.php")
    print("     http-check expect string online")
    for ip in ip_list:
        print(f"     server {ip} {ip}:80 check weight 7")
    print("     stats enable")
    print("     stats uri /admin?stats")
    print("     fullconn 20000")


if __name__ == "__main__":
    with open('./data/testfile', 'r') as f:
        for name in f:
            name = name.strip()
            print("\n")
            create_paths(name)
            print("\n")
            create_use_backend(name)
            print("\n")
            backend_create_app(name)
            print("\n")
            backend_create_survey(name)
            print("\n")
        f.close()
