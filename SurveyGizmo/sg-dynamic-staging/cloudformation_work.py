#!/usr/local/bin/python3
''' All of the work with CloudFormation should be in this module '''

import boto3

CF_CLIENT = boto3.client('cloudformation')


def create_everything_cf_stack(name):
    '''Create the stack for the Everything'''
    response = CF_CLIENT.create_stack(
        StackName=f'{name}-EVERYTHING',
        TemplateURL='https://s3.amazonaws.com/BUCKET/TEMPLATE',
        Parameters=[
            {
                'ParameterKey' : 'AvailZones',
                'ParameterValue' : 'us-east-1a,us-east-1b',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'AMIID',
                'ParameterValue' : 'ami-REDACTED',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'Env',
                'ParameterValue' : f'{name}',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'EnvOverride',
                'ParameterValue' : 'Production',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'GBForPrimary',
                'ParameterValue' : '20',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'GBForSecondary',
                'ParameterValue' : '10',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'IamInstanceProfile',
                'ParameterValue' : 'PROFLE',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'InstanceType',
                'ParameterValue' : 'm4.large',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'KeyName',
                'ParameterValue' : 'KEYNAME',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'MaxCount',
                'ParameterValue' : '2',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'MinCount',
                'ParameterValue' : '0',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'DesiredCapacity',
                'ParameterValue' : '0',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'Name',
                'ParameterValue' : f'{name}Everything',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'OperatorEmail',
                'ParameterValue' : 'YEP@none.com',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'SecurityGroups',
                'ParameterValue' : 'SGID',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'SubnetIds',
                'ParameterValue' : 'subnet-123abc,subnet-456DEF',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'Tier',
                'ParameterValue' : 'Everything',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'TierRole',
                'ParameterValue' : '',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'TierSubRole1',
                'ParameterValue' : '',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'TierSubRole2',
                'ParameterValue' : '',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'Deployment',
                'ParameterValue' : 'CodeDeploy',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'CostCenter',
                'ParameterValue' : 'US',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'EBSOptimized',
                'ParameterValue' : 'false',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'Status',
                'ParameterValue' : 'Serving',
                'UsePreviousValue' : True
            },
        ],
        EnableTerminationProtection=False
    )
    print(response)


def create_web_cf_stack(name):
    '''Create the Web Stack'''
    response = CF_CLIENT.create_stack(
        StackName=f'{name}-WEB',
        TemplateURL='https://s3.amazonaws.com/BUCKET/TEMPLATE',
        Parameters=[
            {
                'ParameterKey' : 'AvailZones',
                'ParameterValue' : 'us-east-1a,us-east-1b',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'AMIID',
                'ParameterValue' : 'ami-AMIID',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'Env',
                'ParameterValue' : f'{name}',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'EnvOverride',
                'ParameterValue' : 'Production',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'GBForPrimary',
                'ParameterValue' : '20',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'GBForSecondary',
                'ParameterValue' : '10',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'IamInstanceProfile',
                'ParameterValue' : 'StagingEverything',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'InstanceType',
                'ParameterValue' : 'm4.large',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'KeyName',
                'ParameterValue' : 'KEYNAME',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'MaxCount',
                'ParameterValue' : '2',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'MinCount',
                'ParameterValue' : '0',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'DesiredCapacity',
                'ParameterValue' : '0',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'Name',
                'ParameterValue' : f'{name}Web',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'OperatorEmail',
                'ParameterValue' : 'YEP@none.com',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'SecurityGroups',
                'ParameterValue' : 'sg-SGID',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'SubnetIds',
                'ParameterValue' : 'subnet-123ABC,subnet-456DEF',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'Tier',
                'ParameterValue' : 'Web',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'TierRole',
                'ParameterValue' : '',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'TierSubRole1',
                'ParameterValue' : '',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'TierSubRole2',
                'ParameterValue' : '',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'Deployment',
                'ParameterValue' : 'CodeDeploy',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'CostCenter',
                'ParameterValue' : 'US',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'EBSOptimized',
                'ParameterValue' : 'false',
                'UsePreviousValue' : True
            },
            {
                'ParameterKey' : 'Status',
                'ParameterValue' : 'Serving',
                'UsePreviousValue' : True
            },
        ],
        EnableTerminationProtection=False
    )
    print(response)

def destroy_cf_stacks(name):
    ''' cf-delete the stacks, both web and app, based on name.'''
    response_everything = CF_CLIENT.delete_stack(StackName=f'{name}-EVERYTHING')
    print(f'EVERYTHING DELETE: {response_everything}')
    response_web = CF_CLIENT.delete_stack(StackName=f'{name}-WEB')
    print(f'WEB DELETE: {response_web}')

if __name__ == "__main__":
    NAME = "NICKTEST"
    create_everything_cf_stack(NAME)
    create_web_cf_stack(NAME)
