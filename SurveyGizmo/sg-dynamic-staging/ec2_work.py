#!/opt/python3/bin/python3
'''EC2 helper functions'''

from time import sleep
import boto3

CLIENT = boto3.client('ec2')

STAGE_EVERYTHING_IAM = 'arn:aws:iam::ACCOUNT:instance-profile/PROFILE1'
STAGE_WEB_IAM = 'arn:aws:iam::ACCOUNT:instance-profile/PROFILE2'


def get_server_ips(name, tier):
    '''This is to get the IP addresses for the Backend creation'''
    server_ips = []
    full_filter = [{'Name':'tag:Name', 'Values':[f'{name}']}, {'Name':'tag:Tier', 'Values':[tier]}]

    response = CLIENT.describe_instances(Filters=full_filter)

    for inst in response['Reservations']:
        server_ips.append(inst['Instances'][0]['PrivateIpAddress'])

    return server_ips


def req_spot_instances(name):
    '''Create 2 Spot Instances for the Staging ENV's'''

    response = CLIENT.request_spot_instances(
        InstanceCount=1,
        LaunchSpecification={
            'IamInstanceProfile': {
                'Arn': f'{STAGE_EVERYTHING_IAM}',
            },
            'ImageId': 'ami-AMIID',
            'InstanceType': 'm4.large',
            'KeyName': 'KEYNAME',
            'Placement': {
                'AvailabilityZone': 'us-east-1a',
            },
            'SecurityGroupIds': [
                'sg-SGID',
            ],
            'SubnetId': 'subnet-123ABC',
        },
        Type='one-time',
    )
    response2 = CLIENT.request_spot_instances(
        InstanceCount=1,
        LaunchSpecification={
            'IamInstanceProfile': {
                'Arn': f'{STAGE_WEB_IAM}',
            },
            'ImageId': 'ami-AMIID',
            'InstanceType': 'm4.large',
            'KeyName': 'KEYNAME',
            'Placement': {
                'AvailabilityZone': 'us-east-1a',
            },
            'SecurityGroupIds': [
                'sg-SGID',
            ],
            'SubnetId': 'subnet-123ABC',
        },
        Type='one-time',
    )

    everything_id = response['SpotInstanceRequests'][0]['SpotInstanceRequestId']
    print(everything_id)
    web_id = response2['SpotInstanceRequests'][0]['SpotInstanceRequestId']
    print(web_id)

    print("Sleeping for 15 seconds to allow the request to be fulfilled")
    sleep(15)

    response3 = CLIENT.describe_spot_instance_requests()

    for request in response3["SpotInstanceRequests"]:
        if request['SpotInstanceRequestId'] == everything_id and request['Status']['Code'] == 'fulfilled':
            tag_spot_instances(name, 'everything', request['InstanceId'])
        elif request['SpotInstanceRequestId'] == web_id and request['Status']['Code'] == 'fulfilled':
            tag_spot_instances(name, 'web', request['InstanceId'])
        else:
            print("Nothing in the requests list that matches the request ID's. Recheck responses for 'fulfilled'")

def tag_spot_instances(name, tier, instance_id):
    '''Add tags to spot instances for puppet et al'''
    instance_tags_everything = [
        {'Key': 'CostCenter', 'Value': 'US'},
        {'Key': 'Name', 'Value': f'Everything-{name}'},
        {'Key': 'Env', 'Value': 'DynamicStaging'},
        {'Key': 'Tier', 'Value': 'Everything'},
        {'Key': 'TierRole', 'Value': ''},
        {'Key': 'TierSubRole1', 'Value': ''},
        {'Key': 'EnvOverride', 'Value': 'Production'},
        {'Key': 'EnvVersion', 'Value': '20150115'},
        {'Key': 'Status', 'Value': 'Serving'}
    ]

    instance_tags_web = [
        {'Key': 'CostCenter', 'Value': 'US'},
        {'Key': 'Name', 'Value': f'Web-{name}'},
        {'Key': 'Env', 'Value': 'DynamicStaging'},
        {'Key': 'Tier', 'Value': 'Web'},
        {'Key': 'TierRole', 'Value': ''},
        {'Key': 'TierSubRole1', 'Value': ''},
        {'Key': 'EnvOverride', 'Value': 'Production'},
        {'Key': 'EnvVersion', 'Value': '20150115'},
        {'Key': 'Status', 'Value': 'Serving'}
    ]

    if tier == 'everything':
        response = CLIENT.create_tags(Resources=[instance_id,], Tags=instance_tags_everything)
        print(response)
    elif tier == 'web':
        response = CLIENT.create_tags(Resources=[instance_id,], Tags=instance_tags_web)
        print(response)
    else:
        print("Tags Spot Instances fell through. Check tier pass through")

