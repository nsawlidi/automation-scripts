#!/bin/bash

usage="$( basename "$0" ) [-h] [-e] [-d] [-n] This program will deploy wordpress
where:
        -h    Show this help text
        -e    Environment name pushing from - vlad, qa, nick.sawlidi
        -d    Deployment Destination - qa, prod, stage(SPECIAL TALK TO IT)
        -n    Build Name
"

ENV_PATH="/home/nick.sawlidi/vhosts/surveygizmo.com"
DEP_DEST="DEV-Test"
DEPLOY_PATH="/home/httpd/vhosts/surveygizmo.com"
BUILDNAME="UNNAMED-BUILD"
APPNAME="Wordpress"

while getopts ":he:d:n:" opt; do
    case $opt in
        h) echo "$usage"
           exit
           ;;
        e) if [ "$OPTARG" == "qa" ] ; then
                ENV_PATH="/home/httpd/vhosts/surveygizmo.com"
            elif [ "$OPTARG" == "vlad" ] ; then
                ENV_PATH="/home/vlad/vhosts/surveygizmo.com"
            elif [ "$OPTARG" == "nick.sawlidi" ] ; then
                ENV_PATH="/home/nick.sawlidi/vhosts/surveygizmo.com"
            fi
            ;;
        d) if [ "$OPTARG" == "qa" ] ; then
            DEP_DEST="DEV-Wordpress"
           elif [ "$OPTARG" == "prod" ] ; then
               if [ "$ENV_PATH" == "vlad" ] ; then
                   echo "You are not allowed to deploy from dev-vlad to PROD"
                   DEP_DEST=""
                   exit 1
               elif [ "$ENV_PATH" != "vlad" ] ; then
                   DEP_DEST="PROD-US"
               fi
           elif [ "$OPTARG" == "stage" ] ; then
               DEP_DEST="StgWP"
               APPNAME="Staging-Wordpress"
           fi
           ;;
       n) BUILDNAME="$OPTARG"
           ;;
       \?) printf "illegal option: -%s\n" "$OPTARG" >&2
           echo "$usage" >&2
           exit 1
           ;;
   esac
done

#Let's begin
echo "Starting deployment from $ENV_PATH to $FINAL_DEPLOY_PATH. AppName: $APPNAME, Deployment Group: $DEP_DEST"

# Push the build to S3 and get the eTag and Version for the deployment
echo "Pushing Build $BUILDNAME from $ENV_PATH"
DEPLOY_PUSH="$( aws deploy push --application-name $APPNAME --s3-location s3://surveygizmo-artifacts/Wordpress/$BUILDNAME.zip --source $ENV_PATH --region us-east-1 --output text )"
ETAG="$( echo $DEPLOY_PUSH|awk '{print $13}' |awk -F"[,=]" '{print $8}' )"
VERSION="$( echo $DEPLOY_PUSH|awk '{print $13}' |awk -F"[,=]" '{print $10}' )"
echo "ETAG: $ETAG - VERSION: $VERSION"

#Deploy the build, process the output for monitoring
echo "Deploying Build $BUILDNAME"
DEPLOYMENT="$( aws deploy create-deployment --application-name $APPNAME --s3-location bucket=surveygizmo-artifacts,key=Wordpress/$BUILDNAME.zip,bundleType=zip,eTag=$ETAG,version=$VERSION --deployment-group-name $DEP_DEST --file-exists-behavior OVERWRITE --ignore-application-stop-failures --region us-east-1 )"
DEPLOYMENT_ID="$( echo $DEPLOYMENT | awk '{print $3}'|tr -d '"' )"
DEPLOYMENT_STATUS="NOTHING"
echo "Deployment ID: $DEPLOYMENT_ID"

# Monitor the output of the get-deployment command until the build is finished.
while [ "$DEPLOYMENT_STATUS" != "Succeeded" ]
do
    sleep 10
    WATCH_COMMAND="$( aws deploy get-deployment --deployment-id $DEPLOYMENT_ID --region us-east-1 )"
    DEPLOYMENT_STATUS="$( echo $WATCH_COMMAND |grep status | awk '{print $7}'|tr -d '"'|tr -d ',' )"
    echo "Deployment Status: $DEPLOYMENT_STATUS"
done

echo "DONE"
