aws cloudformation create-stack \
	--region us-east-1 \
    --timeout-in-minutes 10 \
    --no-disable-rollback \
    --stack-name <NAME> \
    --template-url https://s3.amazonaws.com/sg-cftemplates/AutoscaleSinglegp2DeviceV11NOPIOPSx1AdditionalVolumeCodeDeploy \
    --parameters \
    ParameterKey=AvailZones,ParameterValue=\'<ZONES>\',UsePreviousValue=true \
    ParameterKey=AMIID,ParameterValue=<AMI-ID>,UsePreviousValue=true \
    ParameterKey=Env,ParameterValue=<ENVNAME>,UsePreviousValue=true \
    ParameterKey=EnvOverride,ParameterValue=<ENVOVERRIDE>,UsePreviousValue=true \
    ParameterKey=GBForPrimary,ParameterValue=<INT>,UsePreviousValue=true \
    ParameterKey=GBForSecondary,ParameterValue=<INT>,UsePreviousValue=true \
    ParameterKey=IamInstanceProfile,ParameterValue=<ROLE>,UsePreviousValue=true \
    ParameterKey=InstanceType,ParameterValue=<SIZE>,UsePreviousValue=true \
    ParameterKey=KeyName,ParameterValue=appv3cloud,UsePreviousValue=true \
    ParameterKey=MaxCount,ParameterValue=2,UsePreviousValue=true \
    ParameterKey=MinCount,ParameterValue=0,UsePreviousValue=true \
    ParameterKey=DesiredCapacity,ParameterValue=0,UsePreviousValue=true \
    ParameterKey=Name,ParameterValue=<NAME>,UsePreviousValue=true \
    ParameterKey=OperatorEmail,ParameterValue=it@surveygizmo.com,UsePreviousValue=true \
    ParameterKey=SecurityGroups,ParameterValue=<SGID>,UsePreviousValue=true \
    ParameterKey=SubnetIds,ParameterValue=\'<SUBNET IDS 1 PER AZ\',UsePreviousValue=true \
    ParameterKey=Tier,ParameterValue=<TIER NAME>,UsePreviousValue=true \
    ParameterKey=TierRole,ParameterValue=,UsePreviousValue=true \
    ParameterKey=TierSubRole1,ParameterValue=,UsePreviousValue=true \
    ParameterKey=TierSubRole2,ParameterValue=,UsePreviousValue=true \
    ParameterKey=Deployment,ParameterValue=CodeDeploy,UsePreviousValue=true \
    ParameterKey=CostCenter,ParameterValue=US,UsePreviousValue=true \
    ParameterKey=EBSOptimized,ParameterValue=false,UsePreviousValue=true \
    ParameterKey=Status,ParameterValue=Serving,UsePreviousValue=true
