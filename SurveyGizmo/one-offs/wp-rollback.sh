#!/bin/bash

usage="$( basename "$0" ) [-h] [-e] [-d] [-n] This program will deploy wordpress
where:
        -h    Show this help text
        -d    Deployment Environment - qa, prod, stage(SPECIAL TALK TO IT)
"

DEP_DEST="StgWP"
APPNAME="Staging-Wordpress"
TODAY="$( date +%Y-%m-%dT%T )"
YESTERDAY="$( date -d '24 hours ago' +%Y-%m-%dT%T )"

while getopts ":hd:" opt; do
    case $opt in
        h) echo "$usage"
            exit
            ;;
        d) if [ "$OPTARG" == "qa" ] ; then
                DEP_DEST="DEV-Wordpress"
            elif [ "$OPTARG" == "prod" ] ; then
                DEP_DEST="PROD-US"
            elif [ "$OPTARG" == "stage" ] ; then
                DEP_DEST="StgWP"
                APPNAME="Staging-Wordpress"
            fi
        ;;
        \?) printf "illegal option: -%s\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
        ;;
    esac
done

#Let's begin
echo "Rollback Initiated."

#First Grab the previous deployments
DEP_LIST="$( aws deploy list-deployments --application-name $APPNAME --deployment-group-name $DEP_DEST --include-only-statuses Succeeded --region us-east-1 )"

DEP_ID="$( echo $DEP_LIST | awk '{print $4}' | tr -d '"'|tr -d ',' )"
echo  "Rolling back to deployment: $DEP_ID" 

PREV_DEPLOY="$( aws deploy get-deployment --deployment-id $DEP_ID --region us-east-1 --query "deploymentInfo.previousRevision.s3Location" )"
#echo  "Prev Deploy: $PREV_DEPLOY"
VERSION="$( echo $PREV_DEPLOY |awk '{print $5}' | tr -d '"'|tr -d ',' )"
echo "VERSION: $VERSION"
BUCKET="$( echo $PREV_DEPLOY |awk '{print $7}' | tr -d '"'|tr -d ',' )"
echo "BUCKET: $BUCKET"
KEY="$( echo $PREV_DEPLOY |awk '{print $9}' | tr -d '"'|tr -d ',' )"
echo "KEY: $KEY"
ETAG="$( echo $PREV_DEPLOY |awk '{print $11}' | tr -d '"'|tr -d ',' )"
echo "ETAG: $ETAG"

#Deploy the build, process the output for monitoring
DEPLOYMENT="$( aws deploy create-deployment --application-name $APPNAME --s3-location bucket=$BUCKET,key=$KEY,bundleType=zip,eTag=$ETAG,version=$VERSION --deployment-group-name $DEP_DEST --file-exists-behavior OVERWRITE --ignore-application-stop-failures --region us-east-1 )"
DEPLOYMENT_ID="$( echo $DEPLOYMENT | awk '{print $3}'|tr -d '"' )"
DEPLOYMENT_STATUS="NOTHING"
echo "Deployment ID: $DEPLOYMENT_ID"
 
# Monitor the output of the get-deployment command until the build is finished.
while [ "$DEPLOYMENT_STATUS" != "Succeeded" ]
do
    sleep 10
    WATCH_COMMAND="$( aws deploy get-deployment --deployment-id $DEPLOYMENT_ID --region us-east-1 )"
    DEPLOYMENT_STATUS="$( echo $WATCH_COMMAND |grep status | awk '{print $7}'|tr -d '"'|tr -d ',' )"
    echo "Deployment Status: $DEPLOYMENT_STATUS"
done

echo "DONE"
