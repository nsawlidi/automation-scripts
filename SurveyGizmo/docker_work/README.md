# Basic instructions

1. Change to the `code/app` directory
1. Run: `docker run --rm -v $(pwd):/app prooph/composer:7.2 install` to install lumen and dependencies to code/vendor.
1. `cd ../..` so that you are in the folder with docker-compose.yml
1. Run `docker-compose up` to get 2 containers running - one Web, running NGINX, and one PHP, running php7-fpm and the app

## Notes
1. The web server is located on port 8080, no https at this time - http://localhost:8080
1. If the base.dockerfile is changed (like with a couple of commits) you will need to run `docker-compose build --no-cache` in order to force a complete rebuild

### TODO:
1. Basic dockerfile to get the PHP basics on the php:7-fpm container. There are a nummber of options. - Should be done at this time with the base.dockerfile
1. Determine base level prereq's - basic PHP packages and php OS packages and add to Dockerfile - need time with folks - NS
1. Split out dockerfiles logically (as they are added), along with networks in docker-compose.yml
