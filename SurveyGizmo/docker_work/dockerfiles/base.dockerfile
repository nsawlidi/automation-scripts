FROM php:7-fpm

RUN apt-get update 
RUN apt-get install -y libmcrypt-dev mysql-client libmagickwand-dev libcurl4-openssl-dev --no-install-recommends
RUN pecl update-channels
RUN pecl install channel://pecl.php.net/mcrypt-1.0.1
#RUN pecl install imagick
#RUN docker-php-ext-enable imagick
RUN docker-php-ext-enable mcrypt
RUN docker-php-ext-install mysqli gd mbstring curl zip json opcache
