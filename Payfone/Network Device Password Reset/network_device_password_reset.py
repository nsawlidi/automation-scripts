#!/opt/python2.7/bin/python

from random import choice
from email.mime.text import MIMEText
from fabric.api import env
from fabric.operations import local
from bs4 import BeautifulSoup
import re, smtplib, xmlrpclib, os, pxssh

env.warn_only = True

ADMIN_PWD = ''

CISCO_USERS = []
JUNIPER_USERS = []

CISCO_KV = {}
JUNIPER_KV = {}

CISCO_HOSTS = ['', '', '', '']

# GENERAL FUNCTIONS
def generate_pass(length=12):
    """Function to generate a password"""

    char_set = {'small': 'abcdefghijklmnopqrstuvwxyz',
                'nums': '0123456789',
                'big': 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                'special': '^!$%&()={[]}+~#-_.:,;<>|'
    }

    password = []

    password_characters = [ choice(char_set['big']),
             choice(char_set['big']),
             choice(char_set['big']),
             choice(char_set['small']),
             choice(char_set['small']),
             choice(char_set['small']),
             choice(char_set['nums']),
             choice(char_set['nums']),
             choice(char_set['nums']),
             choice(char_set['special']),
             choice(char_set['special']),
             choice(char_set['special'])
             ]

    while len(password) < length:
        character_pick = choice(password_characters)
        password.append(character_pick)
        password_characters.remove(character_pick)

    return ''.join(password)

def get_users():
    yes_pattern = re.compile('y')

    CONFLUENCE_URL = "https://payfone.jira.com/wiki/rpc/xmlrpc"
    CONFLUENCE_LOGIN = ""
    CONFLUENCE_PASSWORD = ""
    PAGE_ID = ''

    client = xmlrpclib.Server(CONFLUENCE_URL, verbose = 0)
    auth_token = client.confluence2.login(CONFLUENCE_LOGIN, CONFLUENCE_PASSWORD)
    page = client.confluence2.getPage(auth_token, PAGE_ID)
    soup = BeautifulSoup(page['content'])
    client.confluence2.logout(auth_token)

    iteration = 0
    table_length = len(soup.find_all('tr'))
    while iteration <= (table_length - 1):
        if iteration == 0:
            pass
        else:
            cisco_yes_match = re.search(yes_pattern, soup.find_all('tr')[iteration].find_all('td')[11].string)
            if cisco_yes_match:
                global CISCO_USERS
                CISCO_USERS.append(soup.find_all('tr')[iteration].find_all('td')[1].string)
            juniper_yes_match = re.search(yes_pattern, soup.find_all('tr')[iteration].find_all('td')[10].string)
            if juniper_yes_match:
                global JUNIPER_USERS
                JUNIPER_USERS.append(soup.find_all('tr')[iteration].find_all('td')[1].string)
        iteration += 1

def set_passwords():
    global CISCO_KV
    global JUNIPER_KV
    for user in CISCO_USERS:
        if not user in CISCO_KV:
            CISCO_KV[user] = generate_pass()
    for user in JUNIPER_USERS:
        if user in CISCO_KV:
            JUNIPER_KV[user] = CISCO_KV[user]
        else:
            JUNIPER_KV[user] = generate_pass()

def send_user_email(user, password):
    jturner_pattern = re.compile('')
    sskillin_pattern = re.compile('')
    user_email = ""
    payfone_employee = True
    jt_match = re.search(jturner_pattern, user)
    ss_match = re.search(sskillin_pattern, user)
    if jt_match or ss_match:
        payfone_employee = False
    if payfone_employee:
        user_email = '%s@payfone.com' % user
    else:
        user_email = '%s@inetexperts.net' % user
    print(user_email)
    from_addr = 'techops@payfone.com'
    to_addr = [user_email]
    msg = 'Greetings,\n\nYour password has been changed to %s for your login to Payfone\'s network devices. If you have any questions or concerns, please let us know!\n\nThanks, \n\n -The Operations Team at Payfone.' % password
    message = MIMEText(msg)
    message['Subject'] = 'Payfone Password Reset'
    message['From'] = from_addr
    message['To'] = user_email #",".join(to_addr)
    server = smtplib.SMTP('')
    server.sendmail(from_addr, to_addr, message.as_string())
    server.quit()

def send_final_email(users):
    all_users = ",\n".join(users)
    from_addr = 'itsupport@payfone.com'
    to_addr = 'techops@payfone.com'
    msg = 'Greetings,\n\nThe passwords for the Network Devices have been scrambled and updated for the following users:\n%s\n\n\nThanks,\n\n-The Operations Team' % (all_users)
    message = MIMEText(msg)
    message['Subject'] = 'Network Device Password\'s Have Been Reset'
    message['From'] = from_addr
    message['To'] = to_addr
    server = smtplib.SMTP('')
    server.sendmail(from_addr, to_addr, message.as_string())
    server.quit()

def dedupe():
    output = dict((k, [CISCO_KV[k], JUNIPER_KV.get(k)]) for k in CISCO_KV)
    output.update((k, [None, JUNIPER_KV[k]]) for k in JUNIPER_KV if k not in CISCO_KV)
    for user in output:
        if len(output[user]) > 1:
            output[user] = set(output[user])
            output[user] = ''.join(output[user])
    return output

#CISCO FUNCTIONS
def get_cisco_users(host):
    if host == '' or host == '' or host == '':
        c_line_remove_1 = re.compile('admin')
        c_line_remove_2 = re.compile('cisco')
        c_line_remove_3 = re.compile('UserName    Privilege')
        c_line_remove_4 = re.compile('-------------- ---------')
        c_line_remove_5 = re.compile('SW1#')
        c_line_remove_6 = re.compile('show user')
        c_line_remove_7 = re.compile('exit')
        full_path = os.path.abspath('./cisco_users.tmp')
        cisco_user_list = []
        cisco_user_list_tmp = []
        user_list = []
        PROMPT = 'CAB*-SW.*#$'
        cisco_log = open(full_path, 'w')
        switch_conn = pxssh.pxssh(logfile=cisco_log)
        switch_user = 'admin'
        switch_conn.login(host, switch_user, auto_prompt_reset=False)
        switch_conn.sendline('show users accounts')
        switch_conn.logout()
        cisco_log.close()
        file = open(full_path, 'r')
        for line in file:
            user_list.append(line)
        file.close()
        user_list = filter(None, user_list)
        for line in user_list:
            line = line.lstrip().rstrip()
            match1 = re.search(c_line_remove_1, line)
            match2 = re.search(c_line_remove_2, line)
            match3 = re.search(c_line_remove_3, line)
            match4 = re.search(c_line_remove_4, line)
            match5 = re.search(c_line_remove_5, line)
            match6 = re.search(c_line_remove_6, line)
            match7 = re.search(c_line_remove_7, line)
            if not match1 and not match2 and not match3 and not match4 and not match5 and not match6 and not match7:
                cisco_user_list_tmp.append(line)
        cisco_user_list_tmp = filter(None, cisco_user_list_tmp)
        for item in cisco_user_list_tmp:
            cisco_user_list.append(item.split(" ")[0])
        local('rm -rf ~/cisco_users.tmp')
        return cisco_user_list
    else:
        username_pattern = re.compile('username')
        admin_pattern = re.compile('admin')
        runn_pattern = re.compile('runn')
        cisco_user_list = []
        user_list = []
        full_path = os.path.abspath('./cisco_users.tmp')
        cisco_log = open(full_path,'w')
        switch_conn = pxssh.pxssh(logfile=cisco_log)
        initial_prompt = 'DENSWI-.*$'
        pass_prompt = 'Password.*$'
        PROMPT = initial_prompt
        switch_conn.login(host, 'admin', ADMIN_PWD, auto_prompt_reset=False)
        switch_conn.sendline('en')
        PROMPT = pass_prompt
        switch_conn.prompt()
        switch_conn.sendline(ADMIN_PWD)
        PROMPT = initial_prompt
        switch_conn.sendline('sh runn | i username')
        switch_conn.sendline('exit')
        #switch_conn.logout()
        cisco_log.close()
        file = open(full_path)
        for line in file:
            username_match = re.search(username_pattern, line)
            admin_match = re.search(admin_pattern, line)
            runn_match = re.search(runn_pattern, line)
            if username_match and not admin_match and not runn_match:
                line_sp = line.split()
                cisco_user_list.append(line_sp[1])
        file.close()
        local('rm -rf ~/cisco_users.tmp')
        return cisco_user_list

def cisco_password_reset():
    for host in CISCO_HOSTS:
        print "Starting on Cisco Switch %s" % host
        if host == '' or host == '' or host == '':
            initial_prompt = 'CAB*-SW.*$'
            overwrite_prompt = 'Overwrite file startup-config.*'
            PROMPT = initial_prompt
            full_path = os.path.abspath('./cisco_pw_reset.tmp')
            cisco_log = open(full_path, 'w')
            switch_conn = pxssh.pxssh(logfile=cisco_log)
            switch_user = 'admin'
            switch_conn.login(host, switch_user, auto_prompt_reset=False)
            switch_conn.sendline('conf t')
            for user in CISCO_KV:
                print "Changing password for user %s" % user
                new_pwd = CISCO_KV[user]
                switch_conn.sendline('username %s privilege 15 password %s' % (user, new_pwd))
            switch_conn.sendline('end')
            switch_conn.sendline('wr mem')
            PROMPT = overwrite_prompt
            switch_conn.prompt()
            switch_conn.sendline('Y')
            PROMPT = initial_prompt
            switch_conn.sendline('exit')
            #switch_conn.logout()
            local('rm -rf %s' % full_path)
        else:
            full_path = os.path.abspath('./cisco_pw_reset.tmp')
            cisco_log = open(full_path,'w')
            switch_conn = pxssh.pxssh(logfile=cisco_log)
            initial_prompt = 'DENSWI-.*$'
            pass_prompt = 'Password.*$'
            PROMPT = initial_prompt
            switch_conn.login(host, 'admin', ADMIN_PWD, auto_prompt_reset=False)
            switch_conn.sendline('en')
            PROMPT = pass_prompt
            switch_conn.prompt()
            switch_conn.sendline(ADMIN_PWD)
            PROMPT = initial_prompt
            switch_conn.sendline('conf t')
            for user in CISCO_KV:
                print "Changing password for user %s" % user
                new_pwd = CISCO_KV[user]
                switch_conn.sendline('username %s privilege 15 password %s' % (user, new_pwd))
            switch_conn.sendline('end')
            switch_conn.sendline('wr')
            switch_conn.sendline('end')
            #switch_conn.logout()
            local('rm -rf %s' % full_path)

def cisco_delete_users():
    for host in CISCO_HOSTS:
        print "Starting deletion process on %s" % host
        if host == '' or host == '':
            user_deletion_list = []
            current_users = get_cisco_users(host)
            for user in current_users:
                if not user in CISCO_USERS:
                    user_deletion_list.append(user)
                else:
                    pass
            if len(user_deletion_list) > 0:
                initial_prompt = 'CAB*-SW.*$'
                overwrite_prompt = 'Overwrite file startup-config.*'
                PROMPT = initial_prompt
                full_path = os.path.abspath('./cisco_deletion.tmp')
                cisco_log = open(full_path, 'w')
                switch_conn = pxssh.pxssh(logfile=cisco_log)
                switch_user = 'admin'
                switch_conn.login(host, switch_user, auto_prompt_reset=False)
                switch_conn.sendline('conf t')
                for user in user_deletion_list:
                    switch_conn.sendline('no username %s' % user)
                switch_conn.sendline('end')
                PROMPT = overwrite_prompt
                switch_conn.prompt()
                switch_conn.sendline('Y')
                PROMPT = initial_prompt
                switch_conn.sendline('exit')
                #switch_conn.logout()
                local('rm -rf %s' % full_path)
            else:
                print "No users to delete from %s!" % host
        else:
            user_deletion_list = []
            current_users = get_cisco_users(host)
            for user in current_users:
                if not user in CISCO_USERS:
                    user_deletion_list.append(user)
                else:
                    pass
            if len(user_deletion_list) > 0:
                full_path = os.path.abspath('./cisco_deletion.tmp')
                cisco_log = open(full_path,'w')
                switch_conn = pxssh.pxssh(logfile=cisco_log)
                initial_prompt = 'DENSWI-.*$'
                pass_prompt = 'Password.*$'
                PROMPT = initial_prompt
                switch_conn.login(host, 'admin', ADMIN_PWD, auto_prompt_reset=False)
                switch_conn.sendline('en')
                PROMPT = pass_prompt
                switch_conn.prompt()
                switch_conn.sendline(ADMIN_PWD)
                PROMPT = initial_prompt
                switch_conn.sendline('conf t')
                for user in user_deletion_list:
                    switch_conn.sendline('no username %s' % user)
                switch_conn.sendline('end')
                switch_conn.sendline('wr')
                switch_conn.sendline('end')
                #switch_conn.logout()
                local('rm -rf %s' % full_path)
            else:
                print "No users to delete from %s!" % host

# JUNIPER FUNCTIONS
def get_juniper_users():
    """Get the users that need to be changed in the Juniper"""
    remove_pattern_1 = re.compile('Remote')
    remove_pattern_2 = re.compile('!!')
    remove_pattern_3 = re.compile('FW')
    remove_pattern_4 = re.compile('Name')
    remove_pattern_5 = re.compile('--------------------------------')
    netscreen = re.compile('netscreen')
    local('/bin/bash ~/get_juni_users.sh')
    juniper_users = []
    file = open("admin_users.tmp")
    users = []
    for line in file:
        users.append(line)
    file.close()
    for line in users:
        match1 = re.search(remove_pattern_1, line)
        match2 = re.search(remove_pattern_2, line)
        match3 = re.search(remove_pattern_3, line)
        match4 = re.search(remove_pattern_4, line)
        match5 = re.search(remove_pattern_5, line)
        netscreen_match = re.search(netscreen, line)
        if not match1 and not match2 and not match3 and not match4 and not match5 and not netscreen_match:
            juniper_users.append(line.split(" ")[0])
    local('rm -f ~/admin_users.tmp')
    return juniper_users

def juniper_password_reset():
    for user in JUNIPER_KV:
        print "Changing password for %s \n" % user
        new_pwd = JUNIPER_KV[user]
        command_text = 'unset admin user %s\n\nset admin user %s password %s privilege all\n\nsave\n\nexit\n\n' % (user, user, new_pwd)
        file = open('password_change.tmp', 'w')
        file.write(command_text)
        file.close()
        local('/bin/bash ~/juni_password_change.sh')
        local('rm -f password_change.tmp')

def juniper_delete_user():
    user_deletion_list = []
    user_list = get_juniper_users()
    for user in user_list:
        if not user in JUNIPER_USERS:
            user_deletion_list.append(user)
        else:
            pass
    if len(user_deletion_list) > 0:
        for user in user_deletion_list:
            command_text = 'unset admin user %s\n\nsave\n\nexit\n\n' % user
            file = open('password_change.tmp', 'w')
            file.write(command_text)
            file.close()
            local('/bin/bash ~/juni_password_change.sh')
            local('rm -f password_change.tmp')
    else:
        print "No users to delete from the Juniper! \n"

# MAIN
if __name__ == '__main__':
    print "Getting users from Confluence"
    get_users()
    print "Users grabbed, setting passwords"
    set_passwords()
    print "Connecting to the Juniper to reset passwords"
    juniper_password_reset()
    print "Juniper passwords reset, cleaning up the Juniper user list"
    juniper_delete_user()
    print "Juniper complete. Starting the Cisco password reset process"
    cisco_password_reset()
    print "Cisco passwords reset, starting the deletion process"
    cisco_delete_users()
    print "Cisco users cleaned up, preparing emails"
    ALL_USERS = dedupe()
    #print ALL_USERS
    for user in ALL_USERS:
        print 'Sending email to %s' % user
        send_user_email(user, ALL_USERS[user])
    print "Individual emails sent, sending final email"
    send_final_email(ALL_USERS)
