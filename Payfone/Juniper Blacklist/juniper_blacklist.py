#!/opt/python2.7/bin/python

from fabric.operations import local
from fabric.api import env
from bs4 import BeautifulSoup
from datetime import date, timedelta, datetime
from email.mime.text import MIMEText
import re, xmlrpclib, requests, smtplib

env.warn_only = True

CONFLUENCE_URL = "https://payfone.jira.com/wiki/rpc/xmlrpc"
CONFLUENCE_LOGIN = ""
CONFLUENCE_PASSWORD = ""
PAGE_ID = ''

CONF_MASTER_LIST = {}
DSHIELD_LIST = {}
JUNIPER_REMOVAL_LIST = []

EMAIL_BODY = ""

DEBUG = False

def get_master_list():
    client = xmlrpclib.Server(CONFLUENCE_URL, verbose = 0)
    try:
        auth_token = client.confluence2.login(CONFLUENCE_LOGIN, CONFLUENCE_PASSWORD)
    except:
        print "No internet. Bomb out"
    page = client.confluence2.getPage(auth_token, PAGE_ID)
    soup = BeautifulSoup(page['content'])
    client.confluence2.logout(auth_token)
    iteration = 0
    table_length = len(soup.find_all('tr'))
    while iteration <= (table_length - 1):
        if iteration == 0:
            pass
        else:
            ip_address = soup.find_all('tr')[iteration].find_all('td')[0].string
            comment = soup.find_all('tr')[iteration].find_all('td')[1].string + " on"
            date_last_seen = str(soup.find_all('tr')[iteration].find_all('td')[2].time).split("\"")[1]
            global CONF_MASTER_LIST
            CONF_MASTER_LIST[ip_address] = comment + " " + date_last_seen
        iteration += 1

def get_dshield_list():
    global DSHIELD_LIST
    comment_pattern = re.compile('#')
    first_line_pattern = re.compile('Start')
    updated_pattern = re.compile('updated')
    initial_updated = ""
    block_list = []
    block_file = requests.get('http://feeds.dshield.org/block.txt')
    initial_block_list = block_file.text
    initial_block_list = initial_block_list.splitlines()
    initial_block_list = filter(None, initial_block_list)
    for line in initial_block_list:
        updated_match = re.search(updated_pattern, line)
        if updated_match:
            initial_updated = line
        comment_match = re.search(comment_pattern, line)
        first_line_match = re.search(first_line_pattern, line)
        if not first_line_match and not comment_match:
            block_list.append(line)
    initial_updated = initial_updated.split(" ")
    initial_updated = filter(None, initial_updated)
    updated_time = []
    for line in initial_updated:
        comment_match = re.search(comment_pattern, line)
        updated_match = re.search(updated_pattern, line)
        if not comment_match and not updated_match:
            updated_time.append(line)
    updated_time = " ".join(updated_time)
    time_of_update = datetime.strptime(updated_time, '%a %b %d %H:%M:%S %Y %Z')
    updated_date = time_of_update.strftime('%Y-%m-%d')
    for line in block_list:
        tab_pattern = re.compile("\t")
        tab_match = re.search(tab_pattern, line)
        if tab_match:
            ip_address = line.split("\t")[0]
            cidr_netmask = line.split("\t")[2]
            key = str(ip_address) + "/" + str(cidr_netmask)
            comment = "Blocked by DSHIELD on " + str(updated_date)
            DSHIELD_LIST[key] = comment
        else:
            ip_address = line.split(" ")[0]
            cidr_netmask = line.split(" ")[2]
            key = str(ip_address) + "/" + str(cidr_netmask)
            comment = "Blocked by DSHIELD on " + str(updated_date)
            DSHIELD_LIST[key] = comment

def compare_lists():
    global DSHIELD_LIST
    global CONF_MASTER_LIST
    global EMAIL_BODY
    for item in DSHIELD_LIST:
        if item == "<EXTERNAL RANGE>":
            pass
        elif item == "<EXTERNAL RANGE>":
            pass
        elif item in CONF_MASTER_LIST:
            CONF_MASTER_LIST[item] = DSHIELD_LIST[item]
        elif item not in CONF_MASTER_LIST:
            EMAIL_BODY += "Adding %s - %s to Master List\n" % (item,DSHIELD_LIST[item])
            CONF_MASTER_LIST[item] = DSHIELD_LIST[item]
    temp_master_list = CONF_MASTER_LIST
    CONF_MASTER_LIST = {}
    for item in temp_master_list:
        comment_string = temp_master_list[item]
        date_string = comment_string.split(" ")[4]
        today = date.today()
        entry_date = date(int(date_string.split("-")[0]), int(date_string.split("-")[1]), int(date_string.split("-")[2]))
        time_difference = today - entry_date
        if item == "<EXTERNAL RANGE>":
            pass
        elif item == "<EXTERNAL RANGE>":
            pass
        elif str(time_difference) == "0:00:00":
            CONF_MASTER_LIST[item] = temp_master_list[item]
        elif int(str(time_difference).split(" ")[0]) < 30:
            CONF_MASTER_LIST[item] = temp_master_list[item]

def get_juniper_list():
    global EMAIL_BODY
    members_pattern = re.compile('Members')
    local('/bin/bash get_dshield_block.sh')
    blacklist_file = open("juniper_blacklist.tmp")
    juniper_list = []
    for line in blacklist_file:
        members_match = re.search(members_pattern, line)
        if members_match:
            juniper_list.append(line)
    blacklist_file.close()
    local('rm -f juniper_blacklist.tmp')
    blacklist_members = juniper_list[0]
    blacklist_members = blacklist_members.lstrip().rstrip()
    blacklist_members = blacklist_members.split(" ")
    blacklist_members.pop(0)
    initial_juniper_list = []
    for item in blacklist_members:
        item.lstrip("\"").rstrip("\"")
        initial_juniper_list.append(item)
    for item in initial_juniper_list:
        if item in CONF_MASTER_LIST:
            pass
        else:
            EMAIL_BODY += "Removed %s from the Juniper\n" % item
            global JUNIPER_REMOVAL_LIST
            JUNIPER_REMOVAL_LIST.append(item)

def upload_to_juniper():
    file = open('blocklist_commands.txt', 'w')
    file.write('unset group address untrust dshield_blocks clear\n\n')
    for item in JUNIPER_REMOVAL_LIST:
        file.write('unset address untrust %s\n\n' % item)
    file.write('save\n\n')
    for item in CONF_MASTER_LIST:
        name = item
        ip_address = item.split("/")[0]
        netmask = ""
        if item.split("/")[1] == "24":
            netmask = "255.255.255.0"
        comment = CONF_MASTER_LIST[item]
        file.write('set address untrust %s %s %s "%s"\n\n' % (name,ip_address,netmask,comment))
        file.write('set group address untrust dshield_blocks add "%s"\n\n' % name)
    file.write('save\n\nexit\n\n')
    file.close()
    local('/bin/bash upload_blocklist.sh')
    local('rm -f blocklist_commands.txt')

def send_email():
    from_addr = 'itsupport@payfone.com'
    to_addr = 'itsupport@payfone.com'
    if EMAIL_BODY == "":
        global EMAIL_BODY
        EMAIL_BODY = "No changes!"
    msg = 'Greetings,\n\nThe blacklist has been updated.\n\nChanges are:\n\n%s\n\nThanks,\n\n-The Operations Team' % EMAIL_BODY
    message = MIMEText(msg)
    message['Subject'] = 'Juniper Blocklist Update'
    message['From'] = from_addr
    message['To'] = to_addr
    if DEBUG:
        filename = 'blocklist_upload_output.tmp'
        f = file(filename)
        attachment = MIMEText(f.read())
        attachment.add_header('Content-Disposition', 'attachment', filename=filename)
        message.attach(attachment)
    local('rm -f blocklist_upload_output.tmp')
    server = smtplib.SMTP('<MAIL_SERVER>:25')
    server.sendmail(from_addr, to_addr, message.as_string())
    server.quit()
    
def page_post():
    confluence_page_content = "<p>Master Blacklist</p><table><tbody><tr><th>IP Address/Range</th><th>Comment</th><th colspan=\"1\">Date last found</th></tr>"
    for item in CONF_MASTER_LIST:
        ip_address = item
        line = CONF_MASTER_LIST[item].split(" ")
        date_string = line.pop()
        line.pop()
        comment = " ".join(line)
        full_comment = "<tr><td>%s</td><td>%s</td><td colspan=\"1\"><time datetime=\"%s\"></time></td></tr>" % (ip_address,comment,date_string)
        confluence_page_content += full_comment        
    confluence_page_content += "</tbody></table>"
    client = xmlrpclib.Server(CONFLUENCE_URL, verbose = 0)
    auth_token = client.confluence2.login(CONFLUENCE_LOGIN, CONFLUENCE_PASSWORD)
    page = client.confluence2.getPage(auth_token, PAGE_ID)
    page['content'] = confluence_page_content
    result = client.confluence2.storePage(auth_token, page)
    client.confluence2.logout(auth_token)

if __name__ == "__main__":
    print "Getting Confluence Master List"
    get_master_list()
    print "Master List recieved, getting DShield List"
    get_dshield_list()
    print "DShield list recieved, comparing lists"
    compare_lists()
    print "Comparison complete, getting the Juniper list"
    get_juniper_list()
    print "Juniper list recieved and parsed, uploading the changes to the Juniper"
    upload_to_juniper()
    print "Upload to the Juniper done, sending the email"
    send_email()
    print "Email send, uploading the changes to Confluence"
    page_post()
    print "Confluence upload complete. Script complete!"
