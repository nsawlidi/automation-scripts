# Generic Fabric functions
# Version 1.0 by Nicholas Sawlidi 31JAN14
# Updated to version 1.2 by Sawlidi 21MAR14

from fabric.api import run, sudo, env
from fabric.colors import red, cyan
from fabric.operations import local
from getpass import getpass, getuser


def rPrint(color, text):
    """ Print the text in the color listed """
    print(color('%s: %s' % (env.host_string, text)))


def get_user():
    """ Get the user information for sudo commands """
    env.user = getuser()
    rPrint(cyan, "Username is: %s" % env.user)
    sudo_password = getpass("Please enter your sudo password: ")
    env.password = sudo_password

def run_local_bgcommand(command):
    return local('dtach -n `mktemp -u /tmp/dtach.XXXX` %s'  % command)

def process_count(service):
    APPsvc = ['1', '2', '3']
    ORIG_APP_service = False
    running_proc = False
    for s in rapsvc:
        if s == service:
            ORIG_APP_service = True
    if ORIG_APP_service:
        proc_count = run('ps -ef |grep -v grep |grep -v crypt |grep -v puppet |grep %s |wc -l' % service)
        if proc_count != 0:
            running_proc = True
        else:
            running_proc = False
    elif not ORIG_APP_service:
        proc_count = run('ps -ef | grep -v grep | grep %s |wc -l' % service)
        if proc_count != 0:
            running_proc = True
        else:
            running_proc = False
    return running_proc


def kill_process(service):
    rapsvc = ['mte', 'jte', 'ppe']
    ORIG_APP_service = False
    for s in rapsvc:
        if s == service:
            ORIG_APP_service = True
    if ORIG_APP_service:
        running_procs = run(
            'ps -ef |grep -v grep |grep -v crypt |grep -v puppet |grep %s |awk \'{ print $2 }\'' % service)
        for p in running_procs:
            sudo('kill -9 %s' % p)
        if not process_count(service):
            rPrint(cyan, "%s killed." % service)
        else:
            rPrint(red, "Unable to kill %s. Please do it manually." % service)
    elif not ORIG_APP_service:
        running_procs = run('ps -ef |grep -v grep | grep %s |awk \'{ print $2 }\'' % service)
        for p in running_procs:
            sudo('kill -9 %s' % p)
        if not process_count(service):
            rPrint(cyan, "%s killed." % service)
        else:
            rPrint(red, "Unable to kill %s. Please do it manually." % service)


def stop_service(service):
    if process_count(service):
        sudo('/sbin/service %s stop' % service)
        if process_count(service):
            rPrint(red, "%s is not stopping. Hard killing the process.")
            kill_process(service)
    else:
        rPrint(cyan, "%s is not running." % service)


def start_service(service):
    if not process_count(service):
        sudo('/sbin/service %s start' % service)
        if not process_count(service):
            rPrint(red, "Unable to start %s. Please start it manually." % service)
    else:
        rPrint(red, "%s is already running!" % service)


def service_check(service):
    return sudo('service %s status' % service)
