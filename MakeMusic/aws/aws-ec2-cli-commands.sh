#!/bin/bash

#How many servers are you making with the variables below
COUNT=2
#The starting number of the servers
STARTING_NUM=9

# Used for the loop below.
LOOPNUM=$(($STARTING_NUM + $COUNT - 1))

# Image Creation Variables - these will need to be changed
IMAGE_ID="ami-XXXXXXXX"
KEY_NAME="prod-XX"
SUBNET="subnet-XXXXXXXX"

#Shouldn't change.
INSTANCE_TYPE="t2.micro"
SECURITY_GROUPS="sg-XXXXXXXX sg-XXXXXXXX sg-XXXXXXXX"

# Tag Variables - change as needed
NAME="ps-test-"
ENVIRONMENT="MakeMusic-Production"
CREATEDBY="nsawlidi@makemusic.com"
CREATEDDATE=$(date +"%d-%m-%Y")
OWNER="devops"

for ((inc=$STARTING_NUM;inc<=$LOOPNUM;inc++)) 
do
    #Create a name that increments - prodsm01, prodsm02 etc etc
    FULLNAME="$NAME$inc" 
    echo "Creating $FULLNAME server"
    # Create the instance, returning the instance id
    INSTANCE=`aws ec2 run-instances --image-id $IMAGE_ID --instance-type $INSTANCE_TYPE --key-name $KEY_NAME --security-group-ids $SECURITY_GROUPS --subnet-id $SUBNET --user-data file://aws-name-dns-ad.txt --output text --query 'Instances[*].InstanceId'`
    echo "$FULLNAME server created with instance id $INSTANCE"
    # Create the tags on the server
    `aws ec2 create-tags --resource $INSTANCE --tags Key="Name",Value=$FULLNAME`
    `aws ec2 create-tags --resource $INSTANCE --tags Key="Envoronment",Value=$ENVIRONMENT`
    `aws ec2 create-tags --resource $INSTANCE --tags Key="CreatedBy",Value=$CREATEDBY`
    `aws ec2 create-tags --resource $INSTANCE --tags Key="CreatedDate",Value=$CREATEDDATE`
    `aws ec2 create-tags --resource $INSTANCE --tags Key="Owner",Value=$OWNER`
    echo "$FULLNAME tags created"
done
