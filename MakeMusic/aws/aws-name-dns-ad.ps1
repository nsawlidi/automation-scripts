﻿param([string]$UserName = "AD\nsawlidi",
      [string]$PWD = "Password",
      [string]$CompName = "Unknown")

$ADJoinName = $UserName
$ADJoinPassword = ConvertTo-SecureString $PWD -AsPlainText -Force
$ADJoinCred = New-Object System.Management.Automation.PSCredential $ADJoinName, $ADJoinPassword

$DomName = "domain.com"
$OU = "OU=cloud servers,DC=domain,DC=com"

$Network = Get-NetIPConfiguration
$IPAddy = $Network.IPv4Address.IpAddress
$InterfaceIndex = $Network.InterfaceIndex
$DNSServers = ("8.8.8.8", "8.8.4.4")

Set-DnsClientServerAddress -InterfaceIndex $InterfaceIndex -ServerAddresses $DNSServers
Add-Computer -DomainName $DomName -OUPath $OU -Credential $ADJoinCred -NewName $CompName -Restart -Force
