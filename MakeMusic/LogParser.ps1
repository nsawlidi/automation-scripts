$current_date = Get-Date -DisplayHint Date

$From = "$env:computername@makemusic.com"
$To = "none@peaksware.com"
$Subject = "$current_date - Daily Gradebook URL Report"
$SMTPServer = "smtp-relay.gmail.com"
$SMTPPort = "25"

$Date = Get-Date
$Date = $Date.adddays(-2)
$Date2Str = $Date.ToString("yyyyMMdd")

$Files = Get-ChildItem "C:\inetpub\logs\LogFiles\W3SVC2"
$output = @()

ForEach ($File in $Files){
    $FileDate = $File.creationtime
    $CTDate2Str = $FileDate.ToString("yyyyMMdd")
    if ($CTDate2Str -eq $Date2Str) {
        $lines = select-string -Path $File -Pattern "/api/profileV2/GetCustomerProfile" -AllMatches -SimpleMatch
        ForEach ($line in $lines) {
            if ($line.ToString().split()[-1] -eq "-" ) {
                $cip = $line.ToString().split()[8]
                $output += "NO IP GIVEN - C-IP : $cip"
            }
            else {
                $output += $line.ToString().split()[-1]
            }
        }
    }
}
$Body = Out-String -InputObject ($output | Group-Object | Format-List name, count)
#Write-Host $Body
Send-MailMessage -From $From -to $To -Subject $Subject -Body $Body -SmtpServer $SMTPServer -port $SMTPPort
