Import-Module WebAdministration

#Set Params here. Need: ENV ($envparam) - unknown if anything else
#If prod, set env to "", else set to $envparam
$env = "dev"

$Directory = "D:\sites\site"

#Overall TODO: 
# 1.) Set ConnectionStrings based on the ENV entry - DB connections, site connections.
# 2.) Prep script for input into a CloudFormation Template

# Replace the bindings - this has no check to determine if the change is needed! 
function ChangeBindings {
    foreach ($sitename in Get-ChildItem -Path IIS:\Sites -Name) {
        Stop-Website $sitename
        $wsbindings = Get-ItemProperty -Path "IIS:\Sites\$sitename" -Name Bindings
        for ($i = 0;$i -lt ($wsbindings.Collection).length;$i++) {
            if (($wsbindings.Collection[$i]).bindingInformation -Match "443"){
                $newbindingname = ""
                $newbindingname = "*:443:$env$sitename"
                ($wsbindings.Collection[$i]).bindingInformation = $newbindingname
            }
            elseif ($sitename -eq "Default Web Site") {
                $newbindingname = ""
                $newbindingname = "*:8001"
                ($wsbindings.Collection[$i]).bindingInformation = $newbindingname
            }
            else {
                $newbindingname = ""
                $newbindingname = "*:80:$env$sitename"
                ($wsbindings.Collection[$i]).bindingInformation = $newbindingname
            }
        }
        Set-ItemProperty -Path "IIS:\Sites\$sitename" -Name Bindings -Value $wsbindings
        Start-Website $sitename
    }
}

# Recurse into the Sites dir and get a list of all the configs. 
# Proceed to change all strings to the conneciton strings listed in the global params section.
# If not PROD, DELETE newrelic.config - time to clean those out. 
 
# List all files with the .config extension

foreach ($file in Get-ChildItem -path $Directory -Recurse -Include *.config -Name) {
    #First check $env and Filename for newrelic. If they match, REMOVE
    #Else: Read in the file and search for makemusic or smartmusic. if match, change to the correct 
    if ($file -Match "newrelic.config" -AND $env -ne "prod") {
        #Remove-Item $file
        echo "REMOVE: $file"
    }
    else {
        echo ""
        echo "FILE: $file"
        echo ""
        (Get-Content $directory\$file) | ForEach-Object {
            if ($_ -Match "makemusic.com" -OR $_ -Match "smartmusic.com") {
                if ($_ -notmatch "@") {
                    if ($_ -Match "Data Source=" -or $_ -Match "Server=") {
                        echo "DB LINE CHANGE: $_"
                        $DB_STRING = ""
                        $UID_STRING = ""
                        switch($env) {
                            "dev" {
                                if ($_ -match "cs") {
                                    $DB_STRING = "Data Source=DEVDB02-CS.makemusic.com"
                                    $UID_STRING = "User ID=SQLUser; Password=UB125izMas"
                                }
                                else {
                                    $DB_STRING = "Data Source=DEVDB01.makemusic.com"
                                    $UID_STRING = "User ID=SQLUser; Password=UB125izMas"
                                }
                                #Insert replace behaviour here. Check fro Failover Partner - if exists, change replacement line to include.
                            }
                            "uat" {
                                if ($_ -match "cs") {
                                    $DB_STRING = "Data Source=UATDB02-CS.makemusic.com"
                                    $UID_STRING = "User Id=QASQLUser; Password=Dv8aef8M"
                                }
                                else {
                                    $DB_STRING = "Data Source=UATDB01.makemusic.com"
                                    $UID_STRING = "User Id=QASQLUser; Password=Dv8aef8M"
                                }
                                #Insert replace behaviour here. Check fro Failover Partner - if exists, change replacement line to include.
                            }
                            "prod" {
                                if ($_ -match "cs") {
                                    $DB_STRING = "Data Source=PRODDB05-CS.makemusic.com; Failover Partner=PRODDB06-CS.makemusic.com"
                                    $UID_STRING = "User Id=ProdSQLUser; Password=KrjLzfkAbY"
                                }
                                else {
                                    $DB_STRING = "Data Source=PRODDB03.makemusic.com;Failover Partner=PRODDB04.makemusic.com"
                                    $UID_STRING = "User Id=ProdSQLUser; Password=KrjLzfkAbY"
                                }
                                #Insert replace behaviour here. Check fro Failover Partner - if exists, change replacement line to include.
                            }
                            default {
                                echo "Error on DB Switch. Break Break Break"
                                break
                            }
                        }
                    }
                    ElseIf ($_ -Match "makemusic.com") {
                        #echo "MAKEMUSIC LINE CHANGE: $_"
                        #Challenges - the lines with prod URL's have nothing definitive - no prod on the front.
                    }
                    ElseIf ($_ -Match "smartmusic.com") {
                        #echo "SMARTMUSIC LINE CHANGE: $_"
                        #Challenges - the lines with prod URL's have nothing definitive - no prod on the front.
                    }
                    else {
                        echo "I HAVE NO IDEA HOW YOU GOT HERE. NICK FAILED. $_"
                    }
                }
            }
        }
    }
}
